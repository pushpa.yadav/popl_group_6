# Popl Group 6

This is popl-2020 course project for group 6.
It includes below topics to be explored and experiment using popl techniques:
1. Mergesort: https://www.youtube.com/watch?v=FCEJiCanLzU
2. DF Traversal: [here](../blob/master//Final/Code Files/DFT)
3. Factorial: https://youtu.be/KJu0dG0VZLk
 

  
    

| Group members | Roll No |
| ------ | ------ |
| Pushpa Yadav | 2019900034 |
| Shelly Jain | 20171008 |
| Praffullitt Jain | 20171142 |
| Adarsh Dharmadevan | 2018111012 |
