# Depth-First Traversal

This system has been implemented using plain HTML and Javascript. 
To run the web application:
  - Clone this directory
  - Right click on `index.html`
  - Open with web browser (Chrome, Safari etc.)
Note: the code makes use of relative paths.
