// Global variables
var split_state = [];
var merge_state = [[2,3,5,9],[1,4,6,8],[]];
var mergesort_state = 0;
var states = [ [[3,7,4,1,5,2,6,9],[]], [[3,7,4,1,5,2,6,9],[3]], [[3,7,4,1,5,2,6,9],[3,1]], [[3,7,4,1,5,2,6,9],[3,1,0]], [[3,7,4,1,5,2,6,9],[3,1]], [[3,7,4,1,5,2,6,9],[3,1,2]], [[3,7,1,4,5,2,6,9],[3,1]], [[1,3,4,7,5,2,6,9],[3]], [[3,7,4,1,5,2,6,9],[3,5]], [[3,7,4,1,5,2,6,9],[3,5,4]], [[3,7,4,1,2,5,6,9],[3,5]], [[3,7,4,1,2,5,6,9],[3,5,6]], [[3,7,4,1,2,5,6,9],[3,5]], [[3,7,4,1,2,5,6,9],[3]], [[1,2,3,4,5,6,7,9],[]] ];
// 

function hideStartScreen() 
{
    document.getElementById("startscreen").remove();
}

function changeExperimentTo(experiment) 
{
    // Change the active link in navbar
    document.querySelector("nav-item.active").removeAttribute("class");
    document.getElementsByTagName("nav-item")[experiment].className="active";

    // Remove everything inside experiment
    document.querySelector(".page .experiment").remove();
    newdiv = document.createElement("DIV");
    newdiv.className = "experiment";
    document.querySelector(".page").appendChild(newdiv);

    // Change the text and experiement
    paragraphs = document.querySelectorAll(".page .text p");

    // Switching between the experiment machines
    switch(experiment) 
    {
        case 0:
            paragraphs[0].innerText = "Split the array into until each element is a sorted array which can then be merged.";
            paragraphs[1].innerText = "The Split Machine enables us to split any array at any pont into 2 sub arrays.";
            paragraphs[2].innerHTML = "Click at the required position where you want to split the array. <br><br>   Keep on splitting untill all      the subarrays are sorted arrays. <br>    (A unit array is considered a sorted array)."
            document.querySelector(".page").className = "page split";
            // Initial state for the machine
            // It consists of an array split_state which contains the indices of the split points.
            split_state = [];
            // Call to renderSplit() function to acutally render the experiment
            renderSplit();
            break;

        case 1:
            paragraphs[0].innerText = "Merge the two given sorted array into a single sorted array.";
            paragraphs[1].innerText = "The Merge Machine lets us select which arrays first element to be added to the merged array. On repeating this process by selecting the array with the smaller element at each step we ensure that the final merged array is sorted.";
            paragraphs[2].innerHTML = "Click the eighter the left or right given sorted array to remove its first element and added it to the merged array. <br><br>   Repeat this process untill both arrays are empty taking care to select the smaller element at each step. <br>"
            paragraphs[2].innerHTML = "Click at the required position where you want to split the array. <br><br>   Keep on splitting untill all the subarrays are sorted arrays. <br>    (A unit array is considered a sorted array)."
            document.querySelector(".page").className = "page merge";
            // Initial state for the machine
            // It is 3-tuple consisting of 3 lists <l1, l2, l3>. l1 and l2 are the two sorted arrays to be merged and l3 is the merged array.
            merge_state = [[2,3,5,9],[1,4,6,8],[]];
            // Call to renderMerge() function to actually render the experiment
            renderMerge();
            break;

        case 2:
            paragraphs[0].innerText = "Sort the array using merge function.";
            paragraphs[1].innerText = "The Sort Machine enables us to select two arrays and merge into a sorted array provide the two selected arrays are sorted. As initially all the arrays are of unit size, they are sorted by defnition.";
            paragraphs[2].innerHTML = "Click the first array to be merged then click on the second array to be merged then click the merge button, this will merge them. <br><br>   Keep on merging untill you are left with a single sorted array. <br>"
            document.querySelector(".page").className = "page sort";
            // Initial state for the machine
            // It consists of a list of lists and another list for the indices selected for sorting.
            // Initially it will be list of n lists and an empty list. 
            sort_state = [[3],[7],[4],[1],[5],[2],[6],[9]];
            indList = [];
            // Call to renderSort() function to actually render the experiment
            renderSort();
            break;

        case 3:
            paragraphs[0].innerText = "Demonstrate the full mergesort algorithm";
            paragraphs[1].innerText = "You can view the full algorithm step by step.";
            paragraphs[2].innerHTML = "Click the next button to go to next step until it finishes."
            document.querySelector(".page").className = "page mergesort";
            // Initial state for the machine
            mergesort_state = 0;
            // Call to renderSort() function to actually render the experiment
            renderMergesort();
            break;
    }
    
}


function split(evt) 
{
    i = evt.currentTarget.myParam;

    if(split_state.includes(i))
        console.log("already split here!")
    else 
    {
        split_state.push(i);
        renderSplit();
    } 
}


function renderSplit() 
{
    list = [3,7,4,1,5,2,6,9];
    
    //clean experiment
    var rarr = document.querySelectorAll(".page .experiment .array");
    if (rarr[0]) 
        rarr[0].remove();

    var experiment = document.querySelector(".page .experiment");
    var arr = document.createElement("DIV");
    arr.className = "array";
    experiment.appendChild(arr);
    arr = document.querySelector(".page .experiment .array");

    for (i=0;i<8;++i) 
    {
        // Creating a node element
        var node = document.createElement("DIV");

        // Adding click event to node which calls function "split"
        if(i<7) 
        {
            node.myParam = i;
            node.addEventListener("click",split);
        }

        // Adding the array elements to node
        var textnode = document.createTextNode(list[i]);
        node.appendChild(textnode);
        arr.appendChild(node);

        // Adding a spacer element if a split has occured at that index
        if (split_state.includes(i)) 
        {
            node = document.createElement("DIV");
            node.className = "spacer";
            arr.appendChild(node);
        }
    }

    // Display message when final split state has been reached
    if(split_state.length==7)
    {
        var msg = document.createElement("H1");
        msg.appendChild(document.createTextNode("Done!  Proceed to next experiment"));
        experiment.appendChild(msg);
    }
}

function renderMerge()  
{
    // Clean experiment
    document.querySelector(".page .experiment").remove();
    newdiv = document.createElement("DIV");
    newdiv.className = "experiment";
    document.querySelector(".page").appendChild(newdiv);

    // Inserting left array
    var experiment = document.querySelector(".page .experiment");
    var arr = document.createElement("DIV");
    arr.className = "larray";
    arr.myParam = 0;
    arr.addEventListener("click",merge);

    for(n of merge_state[0]) 
    {
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(n);
        node.appendChild(textnode);
        arr.appendChild(node);
    }

    experiment.appendChild(arr);

    // Inserting right array
    arr = document.createElement("DIV");
    arr.className = "rarray";
    arr.myParam = 1;
    arr.addEventListener("click",merge);

    for(n of merge_state[1]) 
    {
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(n);
        node.appendChild(textnode);
        arr.appendChild(node);
    }

    experiment.appendChild(arr);

    // Inserting merged array
    experiment.appendChild(arr);
    arr = document.createElement("DIV");
    arr.className = "marray";

    for(n of merge_state[2]) 
    {
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(n);
        node.appendChild(textnode);
        arr.appendChild(node);
    }

    experiment.appendChild(arr);

    // Display message if experiment ends
    if(merge_state[0].length == 0 && merge_state[1].length == 0) 
    {
        var flag = 0;
        for(i = 1; i<merge_state[2].length;++i) 
        {
            if (merge_state[2][i] < merge_state[2][i-1]) 
            {
                flag = 1;
                break;
            }
        }
        if (flag) 
        {
            var msg = document.createElement("H1");
            msg.innerText="The merged array is not sorted, press the reset button to try again"
            msg.className = "msg";
            experiment.appendChild(msg);
            var btn = document.createElement("DIV");
            btn.innerText="RESET"
            btn.className = "btn";
            btn.addEventListener("click", function() {
                merge_state = [[2,3,5,9],[1,4,6,8],[]];
                renderMerge();
            })
            experiment.appendChild(btn);
        } 
        else 
        {
            msg = document.createElement("H1");
            msg.innerText="Done!    Proceed to next experiment"
            msg.className = "msg";
            experiment.appendChild(msg);
        }
    }
}

function renderSort() 
{
    // Clean experiment
    document.querySelector(".page .experiment").remove();

    newdiv = document.createElement("DIV");
    newdiv.className = "experiment";
    document.querySelector(".page").appendChild(newdiv);

    // Rendering the sort state
    var experiment = document.querySelector(".page .experiment");
    arr1 = document.createElement("DIV");
    arr1.className = "array";
    var l = sort_state.length;

    for(i=0; i<l; ++i)
    {
        for(n of sort_state[i]) 
        {
            // Creating a node1 element
            var node1 = document.createElement("DIV");
            var textnode = document.createTextNode(n);
            node1.appendChild(textnode);
            node1.className = "elm";
            node1.myParam = i;

            // Adding a click event which calls function selectIndex()
            node1.addEventListener("click",selectIndex);
            arr1.appendChild(node1);
        }

        if (i<l-1) 
        {
            // Adding a spacer element
            var node2 = document.createElement("DIV");
            node2.className = "spacer";
            arr1.appendChild(node2);
        }
    }

    // Creating a node3 element
    var node3 = document.createElement("DIV");
    var textnode = document.createTextNode("Merge");
    node3.appendChild(textnode);
    // Adding a click event which calls the function sort_update()
    node3.addEventListener("click",sort_update);
    node3.className = "btn"
    experiment.appendChild(arr1);
    experiment.appendChild(node3);
    
}

function selectIndex(evt) 
{
    // Function used for selecting two indices for sorting
    var i = evt.currentTarget.myParam;
    indList.push(i);
}

function sort_update(evt) 
{
    // larray and rarray are the two selected arrays
    var larray = sort_state[indList[0]];
    var rarray = sort_state[indList[1]];

    // Removing larray from sort_state
    for( var i = 0; i < sort_state.length; i++)
        if ( sort_state[i] === larray)
            sort_state.splice(i, 1);

    // Removing rarray from sort_state
    for( var i = 0; i < sort_state.length; i++)
        if ( sort_state[i] === rarray) 
            sort_state.splice(i, 1);

    // Pushing the sorted array to the sort_state
    sort_state.splice(indList[0],0,mergeTwo(larray, rarray));

    // Removing the already selected indices
    indList = [];

    // Calling renderSort() for rendering the updated state
    renderSort();

}

function mergeTwo(arr1, arr2) 
{
    // Function used to merge two sorted arrays into one sorted array
  let result = [...arr1, ...arr2];
  return result.sort((a,b) => a-b);
}

function merge(evt) 
{
    var n = merge_state[evt.currentTarget.myParam].shift();
    merge_state[2].push(n);
    renderMerge();
}

function renderMergesort() 
{
    // Clean experiment
    document.querySelector(".page .experiment").remove();
    newdiv = document.createElement("DIV");
    newdiv.className = "experiment";
    document.querySelector(".page").appendChild(newdiv);

    var experiment = document.querySelector(".page .experiment");
    var arr = document.createElement("DIV");
    arr.className = "array";
    experiment.appendChild(arr);
    arr = document.querySelector(".page .experiment .array");

    for (i=0;i<8;++i) 
    {
        // Creating a node element
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(states[mergesort_state][0][i]);
        node.appendChild(textnode);
        arr.appendChild(node);
        if (states[mergesort_state][1].includes(i)) 
        {
            // Adding a spacer element
            console.log("inserting spacer at "+(i+1));
            node = document.createElement("DIV");
            node.className = "spacer";
            arr.appendChild(node);
        }
    }
    if (mergesort_state == 14) 
    {
        // Display message when final state is reached
        msg = document.createElement("H1");
        msg.innerText="Done!"
        msg.className = "msg";
        experiment.appendChild(msg);
    }
    else 
    {
        // Rendering a button to go to the next state
        var btn = document.createElement("DIV");
        btn.className = "btn";
        var textnode = document.createTextNode("next");
        btn.appendChild(textnode);
        btn.addEventListener("click",next);
        experiment.appendChild(btn);
    }
}

function next() 
{
    // Function used for rendering the updated state
    mergesort_state = mergesort_state + 1;
    renderMergesort();
}