var nodes = {
  A: ["B", "F"],
  B: ["C"],
  C: ["D", "E"],
  D: [],
  E: [],
  F: ["G", "H"],
  G: [],
  H: [],
};
var parents = {
  A: null,
  B: "AB",
  C: "BC",
  D: "CD",
  E: "CE",
  F: "AF",
  G: "FG",
  H: "FH",
};
var order = ["A", "B", "C", "D", "E", "F", "G", "H"];
var edges = ["AB", "BC", "CD", "CE", "AF", "FG", "FH"];
var coloured = [];
var isAdjacent = ["A"];
function colourNode(event) {
  if (isAdjacent.includes(event.target.id)) {
    coloured.push(event.target.id);
    nodes[event.target.id].forEach((item) => {
      isAdjacent.push(item);
      document.getElementById(item).style.fill = "yellow";
    });
    if (parents[event.target.id] != null) {
      document.getElementById(parents[event.target.id]).style.stroke = "black";
    }
    event.target.style.fill = "red";
  }
  var orderString = "";
  coloured.forEach((item) => {
    orderString = orderString + item;
    orderString = orderString + "->";
  });
  document.getElementById("order").innerHTML = orderString;
}
function resetState() {
  coloured = [];
  isAdjacent = ["A"];
  document.getElementById("order").innerHTML = "";
  order.forEach((item) => {
    document.getElementById(item).style.fill = "#fe9ab8";
  });
  edges.forEach((item) => {
    document.getElementById(item).style.stroke = "#666";
  });
}

document.getElementById("A").addEventListener("click", colourNode);
document.getElementById("B").addEventListener("click", colourNode);
document.getElementById("C").addEventListener("click", colourNode);
document.getElementById("D").addEventListener("click", colourNode);
document.getElementById("E").addEventListener("click", colourNode);
document.getElementById("F").addEventListener("click", colourNode);
document.getElementById("G").addEventListener("click", colourNode);
document.getElementById("H").addEventListener("click", colourNode);
