var order = ["A", "B", "C", "D", "E", "F", "G", "H"];
var edges = ["AB", "BC", "CD", "CE", "AF", "FG", "FH"];
var i = 0;
var j = -1;
coloured = [];
function colournext() {
  if (i >= 8) {
    return;
  }
  document.getElementById(order[i]).style.fill = "red";
  coloured.push(order[i]);
  i++;
  var orderString = "";
  coloured.forEach((item) => {
    orderString = orderString + item;
    orderString = orderString + "->";
  });
  document.getElementById("order").innerHTML = orderString;
  if (j >= 0 && j <= 6) {
    document.getElementById(edges[j]).style.stroke = "black";
  }
  j++;
}
function resetState() {
  document.getElementById("order").innerHTML = "";
  i = 0;
  j = -1;
  coloured = [];

  order.forEach((item) => {
    document.getElementById(item).style.fill = "#fe9ab8";
  });
  edges.forEach((item) => {
    document.getElementById(item).style.stroke = "#666";
  });
}
