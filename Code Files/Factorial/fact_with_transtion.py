#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 22:03:34 2020

@author: pushpa
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 22:00:50 2020

@author: pushpa
"""
import math
from matplotlib.figure import Figure 
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,  
NavigationToolbar2Tk)
from tkinter import  Label, Frame, Entry, Button
from PIL import ImageTk,Image
import tkinter as tk
from tkinter.colorchooser import *
import sys, os
from tkinter import PhotoImage
import networkx as nx 
import functools 
import matplotlib.pyplot as plt
from tkinter import messagebox
from matplotlib.collections import PathCollection
from ast import literal_eval
import math


#global parameters
current_list = []
accumulator=1
number_for_factorial=0
inc =1
selected_list=[]
node_pos={}
color_map = []
dol = {0: [1]}
G=nx.DiGraph(dol)


#helper function for labeling edges
def calculateDistance(x1,y1,x2,y2):
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)  
     return dist  



#it handles button click for trnasition systems index increament.
def index_inc():
    global inc
    if (inc < number_for_factorial):
        inc=inc+1
        listbox.delete(0,tk.END)
        listbox.insert(tk.END, "Index Value" )
        listbox.insert(tk.END, inc)
#it handles button click for trnasition systems index decreament.
def index_dec():
    global inc
    if (inc>0):
        inc=inc-1
        listbox.delete(0,tk.END)
        listbox.insert(tk.END, "Index Value" )
        listbox.insert(tk.END, inc)

#it handles button click for trnasition systems next .
""" it checks if index is equal to number entered then it allowes to decreament and calcuates
    Accumulator value .
    if index is greater than 0 then it allwes to increament it """

def call_next():
    global result
    global number_for_factorial
    global inc
    global accumulator
    global a
    global G
    if (inc==number_for_factorial):
        accumulator=1
        a.clear()
        G.add_node(inc,label={"index="+str(inc):"accumulator="+str(accumulator)},size=15)
        G.add_edge( inc, inc-1)
        pos=nx.bipartite_layout(G,G.nodes)
        nx.draw(G,pos,ax=a,with_labels=True, labels=dict( G.nodes.data("label" )),node_color="white",node_size=600,node_shape='8')

    elif (inc > 0):
        if (accumulator < result):
            accumulator=accumulator*(inc+1)
            a.clear()
            G.add_node(inc,label={"index="+str(inc):"accumulator="+str(accumulator)},size=15)
            G.add_edge( inc, inc-1)
            pos=nx.bipartite_layout(G,G.nodes)
            nx.draw(G,pos,ax=a,with_labels=True, labels=dict( G.nodes.data("label" )),node_color="white", node_size=600,node_shape='8')
    elif (accumulator == result):#achieved factorial
        messagebox.showinfo('Transition', "Transition not possible")
    else:
        messagebox.showinfo('Transition', "Transition not possible")

    canvas.draw()


def label_name(node_pos_list,x,y):
    label_dic={}
    for key ,value in node_pos_list.items():
        distance = calculateDistance(value[0],value[1],x,y)
        label_dic[key]=distance
    key_min = min(label_dic.keys(), key=(lambda k: label_dic[k]))
    return key_min




#helper function for factorial trace calls
def call_label( name, args, kwargs ):
    str_args = [ str(a) for a in args ] +\
               [ str(k) + "=" + str(v) for k,v in kwargs.items() ] 
    return name + "(" + ",".join( str_args ) + ")" 
     
def next_node_id( graph ): 
    if "next_id" in graph.graph: 
        i = graph.graph["next_id"] 
    else: 
        i = 1 
    graph.graph["next_id"] = i+1 
    return i
# trace factorial call and show in canvas
def trace_call_graph(graph, call_stack=[]):
    def trace(func): 
        @functools.wraps(func) 
        def wrapper( *args, **kwargs ): 
            n = next_node_id( graph ) 
            graph.add_node( n, label=call_label( func.__name__, args, kwargs ) )                         
            if len( call_stack ) > 0: 
                g.add_edge( call_stack[-1], n ) 
            call_stack.append( n ) 
            ret = func( *args, **kwargs ) 
            call_stack.pop() 
            return ret 
        return wrapper 
    return trace
g = nx.DiGraph()

#tail recursive call in pythonic way ...
def outer_factorial(n):

    global g
    g = nx.DiGraph()
    @trace_call_graph(g)
    def factorial(n):
        #Recursive Factorial Function
        string = "factorial ( " + str (n) +" )"
        if n==1:
            #to trace the execution
            current_list.append(string)
            return 1
        else:
            #to trace the execution
            current_list.append(string)
            return n *factorial(n-1)
    return factorial(n)

# reset button action handler
def callback():
     global node_pos
     global current_list
     global number_for_factorial
     global index
     global accumulator
     global G
     global red, black
     global result
     global color_map
     red =[]
     black=[]
     G = nx.DiGraph()
     number_for_factorial =0
     accumulator=1
     a.clear()
     color_map = []
     canvas.draw()
     current_list=[]
     info.config(text="result")
     listbox.delete(0,tk.END)
     node_pos={}
     result=0
     return True

#it calculates factorial of given number
def calculate():
    callback() # clean listboxes
    global number_for_factorial
    global inc
    global result
    listbox.insert(tk.END, "Index Value" )
    number_for_factorial = int(entryText.get())
    inc = number_for_factorial
    result=outer_factorial(int(entryText.get()))
    info.config(text=result)
    listbox.insert(tk.END, inc)




#plot the networkx graph
def plot(): 
    global a
    a.clear()
    # the figure that will contain the plot
    #f = Figure(figsize=(7,4), dpi=100)
    #a = f.add_subplot(111)
    pos=nx.spring_layout(g)
    nx.draw( g, pos,ax=a, with_labels=True,
                      labels=dict( g.nodes.data("label" ) ), 
                      node_color="white", 
                      node_size=600 )
  
    



    canvas.draw() 
  

  


#UI initializtion
mw = tk.Tk()
mw.title('Factorial !')
icon=ImageTk.PhotoImage(Image.open("/home/pushpa/Documents/POPL_PROJECT/Sorting_Visualization-master/gui_based_sorts/International_Institute_of_Information_Technology,_Hyderabad_logo.png"))
mw.geometry("900x700")
mw.resizable(1, 1)
entryText = tk.Entry(text=1, bg='blue', fg='white',borderwidth=5,validatecommand=callback)
entryText.place(x = 50, y = 25, width=100, height=25)
listbox1 = tk.Listbox(mw,bg='gray', fg='white',activestyle = 'dotbox',  width= '35',height='90')
listbox1.configure(justify=tk.LEFT)
"""add instructions"""
listbox1.insert(tk.END, "This is a Factorial transition state")
listbox1.insert(tk.END,"machine with Accumulator")
listbox1.insert(tk.END,"                      ")
listbox1.insert(tk.END,"Calculate - calculates and starts")
listbox1.insert(tk.END,"the state machine for factorial")
listbox1.insert(tk.END,"                      ")
listbox1.insert(tk.END,"Plot - shows the factorial of ")
listbox1.insert(tk.END,"a given number a trace calls")
listbox1.insert(tk.END,"                      ")
listbox1.insert(tk.END,"Increment - increaments the index ")
listbox1.insert(tk.END,"                      ")
listbox1.insert(tk.END,"Decrement - decreaments the index ")
listbox1.insert(tk.END,"                      ")
listbox1.insert(tk.END,"NEXT - state machine transition from")
listbox1.insert(tk.END,"       one state to next state")
listbox1.pack(side=tk.RIGHT)


listbox = tk.Listbox(mw,bg='green', fg='white',activestyle = 'dotbox',  width= '15',height='10')
listbox.configure(justify=tk.CENTER)
listbox.pack()
btn = tk.Button(text='Calculate',bg='green',padx=20, command=calculate)
btn.place(x = 50, y = 50, width=100, height=25)
btn = tk.Button(text='Next',bg='green',padx=20, command= call_next)
btn.place(x = 85, y = 100, width=100, height=25)
btn = tk.Button(text='Increment',bg='green',padx=20, command= index_inc)
btn.place(x = 50, y = 75, width=100, height=25)
btn = tk.Button(text='Decrement',bg='red',padx=20, command= index_dec)
btn.place(x = 150, y = 75, width=100, height=25)
info = tk.Label(text='result', bg='white', fg='black')
info.place(x = 150, y = 25, width=100, height=25)
start_over = tk.Button(text='reset',bg='red', padx=20, command=callback)
start_over.place(x = 150, y = 50, width=100, height=25)


f = Figure(figsize=(11,4), dpi=100)
a = f.add_subplot(111)
# button that would displays the plot
plot_button = Button(master = mw,
                     height = 2, 
                     width = 10, 
                    text = "Plot",command = plot)
# place the button 
# into the window 
plot_button.pack()
# a tk.DrawingArea
canvas = FigureCanvasTkAgg(f, master=mw)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
#w = tk.Canvas(mw, width=80, height=40)
#w.pack()

# run the gui 
mw.mainloop()